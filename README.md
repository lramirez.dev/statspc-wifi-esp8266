# StatsPC-WiFi-ESP8266

Código para gadget de escritorio desarrollado en C++ para el microcontrolador ESP8266 en su versión ESP-07.
El Hardware del proyecto es:
- Pantalla Nokia 5110.
- Receptor IR.
- Fuente de poder + Celda Li IonLitio.
- ESP-07 + Funcionalidad WiFi.

El gadget usa un servidor de control de volumen (Solo windows) que debe ejecutarse mediante un script en python. El script para generar el control de volumen fue tomado de la siguiente url: https://github.com/rahmedd/esp8266-WiFi-Volume-Control

Por otra parte, se debe instalar el software OpenHardware (https://openhardwaremonitor.org/) y activar la funcionalidad "Server", con el fin de habilitar la API que comparte, mediante un archivo JSON, los datos de nuestro hardware.