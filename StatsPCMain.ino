#include <Adafruit_GFX.h>      // include Adafruit graphics library
#include <Adafruit_PCD8544.h>  // include Adafruit PCD8544 (Nokia 5110) library

// Librerías IR
#include <IRremoteESP8266.h>
#include <IRrecv.h>
#include <IRutils.h>

// Librerías WiFi y Json
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>
#include <WiFiUdp.h>
#include <NTPClient.h>

// Nokia 5110 LCD module connections (CLK, DIN, D/C, CS, RST)
Adafruit_PCD8544 display = Adafruit_PCD8544(2, 15, 4, 5, 16);

// Sensor IR en GPIO12
const uint16_t Receptor_Pin = 12;

IRrecv irrecv(Receptor_Pin);
decode_results lectura;

// Parámetros conexión wifi
WiFiUDP Udp;
const int serverPort = 20001; //can be changed if port is in use on host pc
const char* ssid = "TU_SSID";
const char* password = "TU_PASSWORD";

// Para reloj
NTPClient timeClient(Udp, "cl.pool.ntp.org", -10799, 60000); // Chile Verano (-10800)
char dayWeek [7][12] = {"Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"};

//Debounce ish?
int prevpercent = 0;
int sensorValue = 5;

long int decCode;

int botonArriba = 0;
int botonAbajo = 0;
int botonEnter = 0;
int menuOpcion = 1;
int nivel = 1;

void setup() {
  Serial.begin(115200);
  // initialize the display
  display.begin();
  display.setContrast(65);
  display.clearDisplay();

  WiFi.begin(ssid, password);
 
  while (WiFi.status() != WL_CONNECTED) {
    display.clearDisplay();
    Serial.println("Conectando...");
    display.println("Conectando...");
    display.display();
    delay(1000);
  }
  display.clearDisplay();
  
  if (WiFi.status() == WL_CONNECTED) {
    Serial.print("Conectado!");
    display.println("Conectado!");
    display.println("Bienvenido a");
    display.println("PC Stats!");
    display.display();
    delay(2000);
    display.clearDisplay();
  }
  
  irrecv.enableIRIn(); // Enable IR reception
  timeClient.begin(); // Enable TimeClient
  
  Serial.println("Iniciando demo Receptor IR...");
  display.println("Iniciando demo Receptor IR...");
  display.display();
  
  display.clearDisplay();
  Serial.println("Esperando una señal...");
  menu(menuOpcion,nivel);
}



void loop() {
  
  if (irrecv.decode(&lectura)) {
        decCode = lectura.value;
        display.clearDisplay();
        display.print("Menu opciones:");
        display.println("");

        Serial.println("");
        Serial.print("Menu opciones:");
        Serial.println("");
        
        botonPresionado(decCode);
        menu(menuOpcion,nivel);
        
        // Prepara próximo valor
        irrecv.resume();
    }
    delay(100);
}

void botonPresionado(long int decCode){
    switch (decCode){
          case 33464415: //ARRIBA
            Serial.println("Arriba");
            if(menuOpcion==2 || menuOpcion==3 || menuOpcion==4 && nivel==1){
              menuOpcion--;
            }
            Serial.print(menuOpcion);
            break;
          case 33478695: //ABAJO
            Serial.println("Abajo");
            if(menuOpcion==1 || menuOpcion==2 || menuOpcion==3 && nivel==1){
              menuOpcion++;
            }
            Serial.print(menuOpcion);
            break;
          case 33480735: //IZQUIERDA
            Serial.println("Izquierda");
            if(menuOpcion==1 && nivel==2){
              nivel--;
            }
            else if(menuOpcion==2 && nivel==2){
              nivel--;
            }
            else if(menuOpcion==3 && nivel==2){
              nivel--;
            }
            else if(nivel==1){
                volumenPCgeneric("baja");
            }
            Serial.print(nivel);
            break;
          case 33460335: //DERECHA
            Serial.println("Derecha");
            if(nivel==1){
                volumenPCgeneric("sube");
            }
            break;
          case 33427695: //ENTER
            Serial.println("Entrar");
            if(menuOpcion==1 && nivel==1){
              nivel++;
            }
            else if(menuOpcion==2 && nivel==1){
              nivel++;
            }
            else if(menuOpcion==3 && nivel==1){
              nivel++;
            }
            Serial.print(nivel);
            break;
          default:
            break;
        }
}

void menu(int menuOpcion, int nivel){
    if(menuOpcion==1){
        if(nivel==1){
            display.clearDisplay();
            display.print(" DonKikon-PC");
            display.println("");
            display.print("");
            display.println("");
            display.print("> Stats PC");
            display.println("");
            display.print("  Volumen PC");
            display.println("");
            display.print("  Reloj");
            display.println("");
            display.print("  Configurar");
            display.display();
        }
        else if(nivel==2){
            display.clearDisplay();
            while(true){
                irrecv.enableIRIn();
                statsPC();
                if (irrecv.decode(&lectura)) {
                    decCode = lectura.value;
                    //Serial.println(decCode);
                    if (decCode==33480735) {
                      botonPresionado(decCode);
                      display.clearDisplay();
                      display.print(" DonKikon-PC");
                      display.println("");
                      display.print("");
                      display.println("");
                      display.print("> Stats PC");
                      display.println("");
                      display.print("  Volumen PC");
                      display.println("");
                      display.print("  Reloj");
                      display.println("");
                      display.print("  Configurar");
                      display.display();
                      break;
                    }
                    else if(decCode==33464415){
                      volumenPCgeneric("sube");
                      continue;
                    }
                    else if(decCode==33478695){
                      volumenPCgeneric("baja");
                      continue;
                    }
                    else{
                      continue;
                    }
                }
            }
        }
        
    }
    else if (menuOpcion==2){
        if(nivel==1){
            display.clearDisplay();
            display.print(" DonKikon-PC");
            display.println("");
            display.print("");
            display.println("");
            display.print("  Stats PC");
            display.println("");
            display.print("> Volumen PC");
            display.println("");
            display.print("  Reloj");
            display.println("");
            display.print("  Configurar");
            display.display();
        }
        else if(nivel==2){
            display.clearDisplay();
            while(true){
                irrecv.enableIRIn();
                volumenPC(decCode);
                decCode=0;
                if (irrecv.decode(&lectura)) {
                    decCode = lectura.value;
                    if (decCode==33480735) {
                      botonPresionado(decCode);
                      display.clearDisplay();
                      display.print(" DonKikon-PC");
                      display.println("");
                      display.print("");
                      display.println("");
                      display.print("  Stats PC");
                      display.println("");
                      display.print("> Volumen PC");
                      display.println("");
                      display.print("  Reloj");
                      display.println("");
                      display.print("  Configurar");
                      display.display();
                      break;
                    }
                    else{
                      continue;
                    }
                }
            }
                        
        }
    }
    else if (menuOpcion==3){
      if(nivel==1){
        display.clearDisplay();
        display.print(" DonKikon-PC");
        display.println("");
        display.print("");
        display.println("");
        display.print("  Stats PC");
        display.println("");
        display.print("  Volumen PC");
        display.println("");
        display.print("> Reloj");
        display.println("");
        display.print("  Configurar");
        display.display();
      }
      else if(nivel==2){
        display.clearDisplay();
        while(true){
                irrecv.enableIRIn();
                reloj(); // Función reloj!
                if (irrecv.decode(&lectura)) {
                    decCode = lectura.value;
                    if (decCode==33480735) {
                      botonPresionado(decCode);
                      display.clearDisplay();
                      display.print(" DonKikon-PC");
                      display.println("");
                      display.print("");
                      display.println("");
                      display.print("  Stats PC");
                      display.println("");
                      display.print("  Volumen PC");
                      display.println("");
                      display.print("> Reloj");
                      display.println("");
                      display.print("  Configurar");
                      display.display();
                      break;
                    }
                    else{
                      continue;
                    }
                }
            }
      }
    }
    else if (menuOpcion==4 && nivel ==1){
        display.clearDisplay();
        display.print(" DonKikon-PC");
        display.println("");
        display.print("");
        display.println("");
        display.print("  Stats PC");
        display.println("");
        display.print("  Volumen PC");
        display.println("");
        display.print("  Reloj");
        display.println("");
        display.print("> Configurar");
        display.display();
    }
    else {
        display.clearDisplay();
        display.print(" DonKikon-PC");
        display.println("");
        display.print("");
        display.println("");
        display.print("> Stats PC");
        display.println("");
        display.print("  Volumen PC");
        display.println("");
        display.print("  Reloj");
        display.println("");
        display.print("  Configurar");
        display.display();
    }
}

void statsPC(){
  if (WiFi.status() == WL_CONNECTED) { //Check WiFi connection status
    HTTPClient http;  //Declare an object of class HTTPClient
 
    http.begin("http://TU_IP:8085/data.json");  //Specify request destination
    int httpCode = http.GET();                                  //Send the request
 
    if (httpCode > 0) { //Check the returning code
 
      //String payload = http.getString();   //Get the request response payload
      //Serial.println(payload);             //Print the response payload

      
      
      DynamicJsonDocument doc(24576);

      
      deserializeJson(doc, http.getString(), DeserializationOption::NestingLimit(12));
      
      
      int id = doc["id"]; // 0
      const char* Text = doc["Text"]; // "Sensor"
      
      JsonObject Children_0 = doc["Children"][0];
      int Children_0_id = Children_0["id"]; // 1
      const char* Children_0_Text = Children_0["Text"]; // "DESKTOP-B3O6IIV"
      
      JsonArray Children_0_Children = Children_0["Children"];
      
      JsonObject Children_0_Children_0 = Children_0_Children[0];
      int Children_0_Children_0_id = Children_0_Children_0["id"]; // 2
      const char* Children_0_Children_0_Text = Children_0_Children_0["Text"]; // "MSI H310M PRO-VDH PLUS (MS-7C09)"
      
      JsonObject Children_0_Children_0_Children_0 = Children_0_Children_0["Children"][0];
      int Children_0_Children_0_Children_0_id = Children_0_Children_0_Children_0["id"]; // 3
      const char* Children_0_Children_0_Children_0_Text = Children_0_Children_0_Children_0["Text"]; // "Nuvoton NCT6797D"
      
      JsonArray Children_0_Children_0_Children_0_Children = Children_0_Children_0_Children_0["Children"];
      
      JsonObject Children_0_Children_0_Children_0_Children_1 = Children_0_Children_0_Children_0_Children[1];
      int Children_0_Children_0_Children_0_Children_1_id = Children_0_Children_0_Children_0_Children_1["id"]; // 19
      const char* Children_0_Children_0_Children_0_Children_1_Text = Children_0_Children_0_Children_0_Children_1["Text"]; // "Temperatures"
      
      JsonArray Children_0_Children_0_Children_0_Children_1_Children = Children_0_Children_0_Children_0_Children_1["Children"];
      
      JsonObject Children_0_Children_0_Children_0_Children_1_Children_0 = Children_0_Children_0_Children_0_Children_1_Children[0];
      int Children_0_Children_0_Children_0_Children_1_Children_0_id = Children_0_Children_0_Children_0_Children_1_Children_0["id"]; // 20
      const char* Children_0_Children_0_Children_0_Children_1_Children_0_Text = Children_0_Children_0_Children_0_Children_1_Children_0["Text"]; // "CPU Core"
      
      const char* Children_0_Children_0_Children_0_Children_1_Children_0_Min = Children_0_Children_0_Children_0_Children_1_Children_0["Min"]; // "41,0 °C"
      const char* Children_0_Children_0_Children_0_Children_1_Children_0_Value = Children_0_Children_0_Children_0_Children_1_Children_0["Value"]; // "44,5 °C"
      const char* Children_0_Children_0_Children_0_Children_1_Children_0_Max = Children_0_Children_0_Children_0_Children_1_Children_0["Max"]; // "66,5 °C"
      const char* Children_0_Children_0_Children_0_Children_1_Children_0_ImageURL = Children_0_Children_0_Children_0_Children_1_Children_0["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_0_Children_0_Children_1_Children_1 = Children_0_Children_0_Children_0_Children_1_Children[1];
      int Children_0_Children_0_Children_0_Children_1_Children_1_id = Children_0_Children_0_Children_0_Children_1_Children_1["id"]; // 21
      const char* Children_0_Children_0_Children_0_Children_1_Children_1_Text = Children_0_Children_0_Children_0_Children_1_Children_1["Text"]; // "Temperature #1"
      
      const char* Children_0_Children_0_Children_0_Children_1_Children_1_Min = Children_0_Children_0_Children_0_Children_1_Children_1["Min"]; // "40,5 °C"
      const char* Children_0_Children_0_Children_0_Children_1_Children_1_Value = Children_0_Children_0_Children_0_Children_1_Children_1["Value"]; // "42,5 °C"
      const char* Children_0_Children_0_Children_0_Children_1_Children_1_Max = Children_0_Children_0_Children_0_Children_1_Children_1["Max"]; // "50,0 °C"
      const char* Children_0_Children_0_Children_0_Children_1_Children_1_ImageURL = Children_0_Children_0_Children_0_Children_1_Children_1["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_0_Children_0_Children_1_Children_2 = Children_0_Children_0_Children_0_Children_1_Children[2];
      int Children_0_Children_0_Children_0_Children_1_Children_2_id = Children_0_Children_0_Children_0_Children_1_Children_2["id"]; // 22
      const char* Children_0_Children_0_Children_0_Children_1_Children_2_Text = Children_0_Children_0_Children_0_Children_1_Children_2["Text"]; // "Temperature #3"
      
      const char* Children_0_Children_0_Children_0_Children_1_Children_2_Min = Children_0_Children_0_Children_0_Children_1_Children_2["Min"]; // "39,0 °C"
      const char* Children_0_Children_0_Children_0_Children_1_Children_2_Value = Children_0_Children_0_Children_0_Children_1_Children_2["Value"]; // "41,0 °C"
      const char* Children_0_Children_0_Children_0_Children_1_Children_2_Max = Children_0_Children_0_Children_0_Children_1_Children_2["Max"]; // "44,0 °C"
      const char* Children_0_Children_0_Children_0_Children_1_Children_2_ImageURL = Children_0_Children_0_Children_0_Children_1_Children_2["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_0_Children_0_Children_1_Children_3 = Children_0_Children_0_Children_0_Children_1_Children[3];
      int Children_0_Children_0_Children_0_Children_1_Children_3_id = Children_0_Children_0_Children_0_Children_1_Children_3["id"]; // 23
      const char* Children_0_Children_0_Children_0_Children_1_Children_3_Text = Children_0_Children_0_Children_0_Children_1_Children_3["Text"]; // "Temperature #4"
      
      const char* Children_0_Children_0_Children_0_Children_1_Children_3_Min = Children_0_Children_0_Children_0_Children_1_Children_3["Min"]; // "109,0 °C"
      const char* Children_0_Children_0_Children_0_Children_1_Children_3_Value = Children_0_Children_0_Children_0_Children_1_Children_3["Value"]; // "109,0 °C"
      const char* Children_0_Children_0_Children_0_Children_1_Children_3_Max = Children_0_Children_0_Children_0_Children_1_Children_3["Max"]; // "110,0 °C"
      const char* Children_0_Children_0_Children_0_Children_1_Children_3_ImageURL = Children_0_Children_0_Children_0_Children_1_Children_3["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_0_Children_0_Children_1_Children_4 = Children_0_Children_0_Children_0_Children_1_Children[4];
      int Children_0_Children_0_Children_0_Children_1_Children_4_id = Children_0_Children_0_Children_0_Children_1_Children_4["id"]; // 24
      const char* Children_0_Children_0_Children_0_Children_1_Children_4_Text = Children_0_Children_0_Children_0_Children_1_Children_4["Text"]; // "Temperature #5"
      
      const char* Children_0_Children_0_Children_0_Children_1_Children_4_Min = Children_0_Children_0_Children_0_Children_1_Children_4["Min"]; // "112,0 °C"
      const char* Children_0_Children_0_Children_0_Children_1_Children_4_Value = Children_0_Children_0_Children_0_Children_1_Children_4["Value"]; // "113,0 °C"
      const char* Children_0_Children_0_Children_0_Children_1_Children_4_Max = Children_0_Children_0_Children_0_Children_1_Children_4["Max"]; // "113,0 °C"
      const char* Children_0_Children_0_Children_0_Children_1_Children_4_ImageURL = Children_0_Children_0_Children_0_Children_1_Children_4["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_0_Children_0_Children_1_Children_5 = Children_0_Children_0_Children_0_Children_1_Children[5];
      int Children_0_Children_0_Children_0_Children_1_Children_5_id = Children_0_Children_0_Children_0_Children_1_Children_5["id"]; // 25
      const char* Children_0_Children_0_Children_0_Children_1_Children_5_Text = Children_0_Children_0_Children_0_Children_1_Children_5["Text"]; // "Temperature #6"
      
      const char* Children_0_Children_0_Children_0_Children_1_Children_5_Min = Children_0_Children_0_Children_0_Children_1_Children_5["Min"]; // "-3,0 °C"
      const char* Children_0_Children_0_Children_0_Children_1_Children_5_Value = Children_0_Children_0_Children_0_Children_1_Children_5["Value"]; // "-3,0 °C"
      const char* Children_0_Children_0_Children_0_Children_1_Children_5_Max = Children_0_Children_0_Children_0_Children_1_Children_5["Max"]; // "-3,0 °C"
      const char* Children_0_Children_0_Children_0_Children_1_Children_5_ImageURL = Children_0_Children_0_Children_0_Children_1_Children_5["ImageURL"]; // "images/transparent.png"
      
      const char* Children_0_Children_0_Children_0_Children_1_Min = Children_0_Children_0_Children_0_Children_1["Min"]; // ""
      const char* Children_0_Children_0_Children_0_Children_1_Value = Children_0_Children_0_Children_0_Children_1["Value"]; // ""
      const char* Children_0_Children_0_Children_0_Children_1_Max = Children_0_Children_0_Children_0_Children_1["Max"]; // ""
      const char* Children_0_Children_0_Children_0_Children_1_ImageURL = Children_0_Children_0_Children_0_Children_1["ImageURL"]; // "images_icon/temperature.png"
      
      JsonObject Children_0_Children_0_Children_0_Children_2 = Children_0_Children_0_Children_0_Children[2];
      int Children_0_Children_0_Children_0_Children_2_id = Children_0_Children_0_Children_0_Children_2["id"]; // 26
      const char* Children_0_Children_0_Children_0_Children_2_Text = Children_0_Children_0_Children_0_Children_2["Text"]; // "Fans"
      
      JsonObject Children_0_Children_0_Children_0_Children_2_Children_0 = Children_0_Children_0_Children_0_Children_2["Children"][0];
      int Children_0_Children_0_Children_0_Children_2_Children_0_id = Children_0_Children_0_Children_0_Children_2_Children_0["id"]; // 27
      const char* Children_0_Children_0_Children_0_Children_2_Children_0_Text = Children_0_Children_0_Children_0_Children_2_Children_0["Text"]; // "Fan #2"
      
      const char* Children_0_Children_0_Children_0_Children_2_Children_0_Min = Children_0_Children_0_Children_0_Children_2_Children_0["Min"]; // "983 RPM"
      const char* Children_0_Children_0_Children_0_Children_2_Children_0_Value = Children_0_Children_0_Children_0_Children_2_Children_0["Value"]; // "989 RPM"
      const char* Children_0_Children_0_Children_0_Children_2_Children_0_Max = Children_0_Children_0_Children_0_Children_2_Children_0["Max"]; // "2049 RPM"
      const char* Children_0_Children_0_Children_0_Children_2_Children_0_ImageURL = Children_0_Children_0_Children_0_Children_2_Children_0["ImageURL"]; // "images/transparent.png"
      
      const char* Children_0_Children_0_Children_0_Children_2_Min = Children_0_Children_0_Children_0_Children_2["Min"]; // ""
      const char* Children_0_Children_0_Children_0_Children_2_Value = Children_0_Children_0_Children_0_Children_2["Value"]; // ""
      const char* Children_0_Children_0_Children_0_Children_2_Max = Children_0_Children_0_Children_0_Children_2["Max"]; // ""
      const char* Children_0_Children_0_Children_0_Children_2_ImageURL = Children_0_Children_0_Children_0_Children_2["ImageURL"]; // "images_icon/fan.png"
      
      JsonObject Children_0_Children_0_Children_0_Children_3 = Children_0_Children_0_Children_0_Children[3];
      int Children_0_Children_0_Children_0_Children_3_id = Children_0_Children_0_Children_0_Children_3["id"]; // 28
      const char* Children_0_Children_0_Children_0_Children_3_Text = Children_0_Children_0_Children_0_Children_3["Text"]; // "Controls"
      
      JsonArray Children_0_Children_0_Children_0_Children_3_Children = Children_0_Children_0_Children_0_Children_3["Children"];
      
      JsonObject Children_0_Children_0_Children_0_Children_3_Children_0 = Children_0_Children_0_Children_0_Children_3_Children[0];
      int Children_0_Children_0_Children_0_Children_3_Children_0_id = Children_0_Children_0_Children_0_Children_3_Children_0["id"]; // 29
      const char* Children_0_Children_0_Children_0_Children_3_Children_0_Text = Children_0_Children_0_Children_0_Children_3_Children_0["Text"]; // "Fan Control #1"
      
      const char* Children_0_Children_0_Children_0_Children_3_Children_0_Min = Children_0_Children_0_Children_0_Children_3_Children_0["Min"]; // "51,4 %"
      const char* Children_0_Children_0_Children_0_Children_3_Children_0_Value = Children_0_Children_0_Children_0_Children_3_Children_0["Value"]; // "56,5 %"
      const char* Children_0_Children_0_Children_0_Children_3_Children_0_Max = Children_0_Children_0_Children_0_Children_3_Children_0["Max"]; // "82,0 %"
      const char* Children_0_Children_0_Children_0_Children_3_Children_0_ImageURL = Children_0_Children_0_Children_0_Children_3_Children_0["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_0_Children_0_Children_3_Children_1 = Children_0_Children_0_Children_0_Children_3_Children[1];
      int Children_0_Children_0_Children_0_Children_3_Children_1_id = Children_0_Children_0_Children_0_Children_3_Children_1["id"]; // 30
      const char* Children_0_Children_0_Children_0_Children_3_Children_1_Text = Children_0_Children_0_Children_0_Children_3_Children_1["Text"]; // "Fan Control #2"
      
      const char* Children_0_Children_0_Children_0_Children_3_Children_1_Min = Children_0_Children_0_Children_0_Children_3_Children_1["Min"]; // "17,6 %"
      const char* Children_0_Children_0_Children_0_Children_3_Children_1_Value = Children_0_Children_0_Children_0_Children_3_Children_1["Value"]; // "24,3 %"
      const char* Children_0_Children_0_Children_0_Children_3_Children_1_Max = Children_0_Children_0_Children_0_Children_3_Children_1["Max"]; // "57,6 %"
      const char* Children_0_Children_0_Children_0_Children_3_Children_1_ImageURL = Children_0_Children_0_Children_0_Children_3_Children_1["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_0_Children_0_Children_3_Children_2 = Children_0_Children_0_Children_0_Children_3_Children[2];
      int Children_0_Children_0_Children_0_Children_3_Children_2_id = Children_0_Children_0_Children_0_Children_3_Children_2["id"]; // 31
      const char* Children_0_Children_0_Children_0_Children_3_Children_2_Text = Children_0_Children_0_Children_0_Children_3_Children_2["Text"]; // "Fan Control #3"
      
      const char* Children_0_Children_0_Children_0_Children_3_Children_2_Min = Children_0_Children_0_Children_0_Children_3_Children_2["Min"]; // "0,0 %"
      const char* Children_0_Children_0_Children_0_Children_3_Children_2_Value = Children_0_Children_0_Children_0_Children_3_Children_2["Value"]; // "0,0 %"
      const char* Children_0_Children_0_Children_0_Children_3_Children_2_Max = Children_0_Children_0_Children_0_Children_3_Children_2["Max"]; // "0,0 %"
      const char* Children_0_Children_0_Children_0_Children_3_Children_2_ImageURL = Children_0_Children_0_Children_0_Children_3_Children_2["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_0_Children_0_Children_3_Children_3 = Children_0_Children_0_Children_0_Children_3_Children[3];
      int Children_0_Children_0_Children_0_Children_3_Children_3_id = Children_0_Children_0_Children_0_Children_3_Children_3["id"]; // 32
      const char* Children_0_Children_0_Children_0_Children_3_Children_3_Text = Children_0_Children_0_Children_0_Children_3_Children_3["Text"]; // "Fan Control #4"
      
      const char* Children_0_Children_0_Children_0_Children_3_Children_3_Min = Children_0_Children_0_Children_0_Children_3_Children_3["Min"]; // "0,0 %"
      const char* Children_0_Children_0_Children_0_Children_3_Children_3_Value = Children_0_Children_0_Children_0_Children_3_Children_3["Value"]; // "0,0 %"
      const char* Children_0_Children_0_Children_0_Children_3_Children_3_Max = Children_0_Children_0_Children_0_Children_3_Children_3["Max"]; // "0,0 %"
      const char* Children_0_Children_0_Children_0_Children_3_Children_3_ImageURL = Children_0_Children_0_Children_0_Children_3_Children_3["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_0_Children_0_Children_3_Children_4 = Children_0_Children_0_Children_0_Children_3_Children[4];
      int Children_0_Children_0_Children_0_Children_3_Children_4_id = Children_0_Children_0_Children_0_Children_3_Children_4["id"]; // 33
      const char* Children_0_Children_0_Children_0_Children_3_Children_4_Text = Children_0_Children_0_Children_0_Children_3_Children_4["Text"]; // "Fan Control #5"
      
      const char* Children_0_Children_0_Children_0_Children_3_Children_4_Min = Children_0_Children_0_Children_0_Children_3_Children_4["Min"]; // "0,0 %"
      const char* Children_0_Children_0_Children_0_Children_3_Children_4_Value = Children_0_Children_0_Children_0_Children_3_Children_4["Value"]; // "0,0 %"
      const char* Children_0_Children_0_Children_0_Children_3_Children_4_Max = Children_0_Children_0_Children_0_Children_3_Children_4["Max"]; // "0,0 %"
      const char* Children_0_Children_0_Children_0_Children_3_Children_4_ImageURL = Children_0_Children_0_Children_0_Children_3_Children_4["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_0_Children_0_Children_3_Children_5 = Children_0_Children_0_Children_0_Children_3_Children[5];
      int Children_0_Children_0_Children_0_Children_3_Children_5_id = Children_0_Children_0_Children_0_Children_3_Children_5["id"]; // 34
      const char* Children_0_Children_0_Children_0_Children_3_Children_5_Text = Children_0_Children_0_Children_0_Children_3_Children_5["Text"]; // "Fan Control #6"
      
      const char* Children_0_Children_0_Children_0_Children_3_Children_5_Min = Children_0_Children_0_Children_0_Children_3_Children_5["Min"]; // "49,8 %"
      const char* Children_0_Children_0_Children_0_Children_3_Children_5_Value = Children_0_Children_0_Children_0_Children_3_Children_5["Value"]; // "49,8 %"
      const char* Children_0_Children_0_Children_0_Children_3_Children_5_Max = Children_0_Children_0_Children_0_Children_3_Children_5["Max"]; // "49,8 %"
      const char* Children_0_Children_0_Children_0_Children_3_Children_5_ImageURL = Children_0_Children_0_Children_0_Children_3_Children_5["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_0_Children_0_Children_3_Children_6 = Children_0_Children_0_Children_0_Children_3_Children[6];
      int Children_0_Children_0_Children_0_Children_3_Children_6_id = Children_0_Children_0_Children_0_Children_3_Children_6["id"]; // 35
      const char* Children_0_Children_0_Children_0_Children_3_Children_6_Text = Children_0_Children_0_Children_0_Children_3_Children_6["Text"]; // "Fan Control #7"
      
      const char* Children_0_Children_0_Children_0_Children_3_Children_6_Min = Children_0_Children_0_Children_0_Children_3_Children_6["Min"]; // "49,8 %"
      const char* Children_0_Children_0_Children_0_Children_3_Children_6_Value = Children_0_Children_0_Children_0_Children_3_Children_6["Value"]; // "49,8 %"
      const char* Children_0_Children_0_Children_0_Children_3_Children_6_Max = Children_0_Children_0_Children_0_Children_3_Children_6["Max"]; // "49,8 %"
      const char* Children_0_Children_0_Children_0_Children_3_Children_6_ImageURL = Children_0_Children_0_Children_0_Children_3_Children_6["ImageURL"]; // "images/transparent.png"
      
      const char* Children_0_Children_0_Children_0_Children_3_Min = Children_0_Children_0_Children_0_Children_3["Min"]; // ""
      const char* Children_0_Children_0_Children_0_Children_3_Value = Children_0_Children_0_Children_0_Children_3["Value"]; // ""
      const char* Children_0_Children_0_Children_0_Children_3_Max = Children_0_Children_0_Children_0_Children_3["Max"]; // ""
      const char* Children_0_Children_0_Children_0_Children_3_ImageURL = Children_0_Children_0_Children_0_Children_3["ImageURL"]; // "images_icon/control.png"
      
      const char* Children_0_Children_0_Children_0_Min = Children_0_Children_0_Children_0["Min"]; // ""
      const char* Children_0_Children_0_Children_0_Value = Children_0_Children_0_Children_0["Value"]; // ""
      const char* Children_0_Children_0_Children_0_Max = Children_0_Children_0_Children_0["Max"]; // ""
      const char* Children_0_Children_0_Children_0_ImageURL = Children_0_Children_0_Children_0["ImageURL"]; // "images_icon/chip.png"
      
      const char* Children_0_Children_0_Min = Children_0_Children_0["Min"]; // ""
      const char* Children_0_Children_0_Value = Children_0_Children_0["Value"]; // ""
      const char* Children_0_Children_0_Max = Children_0_Children_0["Max"]; // ""
      const char* Children_0_Children_0_ImageURL = Children_0_Children_0["ImageURL"]; // "images_icon/mainboard.png"
      
      JsonObject Children_0_Children_1 = Children_0_Children[1];
      int Children_0_Children_1_id = Children_0_Children_1["id"]; // 36
      const char* Children_0_Children_1_Text = Children_0_Children_1["Text"]; // "Intel Core i3-9100F"
      
      JsonArray Children_0_Children_1_Children = Children_0_Children_1["Children"];
      
      JsonObject Children_0_Children_1_Children_0 = Children_0_Children_1_Children[0];
      int Children_0_Children_1_Children_0_id = Children_0_Children_1_Children_0["id"]; // 37
      const char* Children_0_Children_1_Children_0_Text = Children_0_Children_1_Children_0["Text"]; // "Clocks"
      
      JsonArray Children_0_Children_1_Children_0_Children = Children_0_Children_1_Children_0["Children"];
      
      JsonObject Children_0_Children_1_Children_0_Children_0 = Children_0_Children_1_Children_0_Children[0];
      int Children_0_Children_1_Children_0_Children_0_id = Children_0_Children_1_Children_0_Children_0["id"]; // 38
      const char* Children_0_Children_1_Children_0_Children_0_Text = Children_0_Children_1_Children_0_Children_0["Text"]; // "Bus Speed"
      
      const char* Children_0_Children_1_Children_0_Children_0_Min = Children_0_Children_1_Children_0_Children_0["Min"]; // "100,0 MHz"
      const char* Children_0_Children_1_Children_0_Children_0_Value = Children_0_Children_1_Children_0_Children_0["Value"]; // "100,0 MHz"
      const char* Children_0_Children_1_Children_0_Children_0_Max = Children_0_Children_1_Children_0_Children_0["Max"]; // "100,0 MHz"
      const char* Children_0_Children_1_Children_0_Children_0_ImageURL = Children_0_Children_1_Children_0_Children_0["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_1_Children_0_Children_1 = Children_0_Children_1_Children_0_Children[1];
      int Children_0_Children_1_Children_0_Children_1_id = Children_0_Children_1_Children_0_Children_1["id"]; // 39
      const char* Children_0_Children_1_Children_0_Children_1_Text = Children_0_Children_1_Children_0_Children_1["Text"]; // "CPU Core #1"
      
      const char* Children_0_Children_1_Children_0_Children_1_Min = Children_0_Children_1_Children_0_Children_1["Min"]; // "800,0 MHz"
      const char* Children_0_Children_1_Children_0_Children_1_Value = Children_0_Children_1_Children_0_Children_1["Value"]; // "1200,0 MHz"
      const char* Children_0_Children_1_Children_0_Children_1_Max = Children_0_Children_1_Children_0_Children_1["Max"]; // "4100,0 MHz"
      const char* Children_0_Children_1_Children_0_Children_1_ImageURL = Children_0_Children_1_Children_0_Children_1["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_1_Children_0_Children_2 = Children_0_Children_1_Children_0_Children[2];
      int Children_0_Children_1_Children_0_Children_2_id = Children_0_Children_1_Children_0_Children_2["id"]; // 40
      const char* Children_0_Children_1_Children_0_Children_2_Text = Children_0_Children_1_Children_0_Children_2["Text"]; // "CPU Core #2"
      
      const char* Children_0_Children_1_Children_0_Children_2_Min = Children_0_Children_1_Children_0_Children_2["Min"]; // "800,0 MHz"
      const char* Children_0_Children_1_Children_0_Children_2_Value = Children_0_Children_1_Children_0_Children_2["Value"]; // "1200,0 MHz"
      const char* Children_0_Children_1_Children_0_Children_2_Max = Children_0_Children_1_Children_0_Children_2["Max"]; // "4100,0 MHz"
      const char* Children_0_Children_1_Children_0_Children_2_ImageURL = Children_0_Children_1_Children_0_Children_2["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_1_Children_0_Children_3 = Children_0_Children_1_Children_0_Children[3];
      int Children_0_Children_1_Children_0_Children_3_id = Children_0_Children_1_Children_0_Children_3["id"]; // 41
      const char* Children_0_Children_1_Children_0_Children_3_Text = Children_0_Children_1_Children_0_Children_3["Text"]; // "CPU Core #3"
      
      const char* Children_0_Children_1_Children_0_Children_3_Min = Children_0_Children_1_Children_0_Children_3["Min"]; // "800,0 MHz"
      const char* Children_0_Children_1_Children_0_Children_3_Value = Children_0_Children_1_Children_0_Children_3["Value"]; // "1200,0 MHz"
      const char* Children_0_Children_1_Children_0_Children_3_Max = Children_0_Children_1_Children_0_Children_3["Max"]; // "4100,0 MHz"
      const char* Children_0_Children_1_Children_0_Children_3_ImageURL = Children_0_Children_1_Children_0_Children_3["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_1_Children_0_Children_4 = Children_0_Children_1_Children_0_Children[4];
      int Children_0_Children_1_Children_0_Children_4_id = Children_0_Children_1_Children_0_Children_4["id"]; // 42
      const char* Children_0_Children_1_Children_0_Children_4_Text = Children_0_Children_1_Children_0_Children_4["Text"]; // "CPU Core #4"
      
      const char* Children_0_Children_1_Children_0_Children_4_Min = Children_0_Children_1_Children_0_Children_4["Min"]; // "800,0 MHz"
      const char* Children_0_Children_1_Children_0_Children_4_Value = Children_0_Children_1_Children_0_Children_4["Value"]; // "1200,0 MHz"
      const char* Children_0_Children_1_Children_0_Children_4_Max = Children_0_Children_1_Children_0_Children_4["Max"]; // "4100,0 MHz"
      const char* Children_0_Children_1_Children_0_Children_4_ImageURL = Children_0_Children_1_Children_0_Children_4["ImageURL"]; // "images/transparent.png"
      
      const char* Children_0_Children_1_Children_0_Min = Children_0_Children_1_Children_0["Min"]; // ""
      const char* Children_0_Children_1_Children_0_Value = Children_0_Children_1_Children_0["Value"]; // ""
      const char* Children_0_Children_1_Children_0_Max = Children_0_Children_1_Children_0["Max"]; // ""
      const char* Children_0_Children_1_Children_0_ImageURL = Children_0_Children_1_Children_0["ImageURL"]; // "images_icon/clock.png"
      
      JsonObject Children_0_Children_1_Children_1 = Children_0_Children_1_Children[1];
      int Children_0_Children_1_Children_1_id = Children_0_Children_1_Children_1["id"]; // 43
      const char* Children_0_Children_1_Children_1_Text = Children_0_Children_1_Children_1["Text"]; // "Temperatures"
      
      JsonArray Children_0_Children_1_Children_1_Children = Children_0_Children_1_Children_1["Children"];
      
      JsonObject Children_0_Children_1_Children_1_Children_0 = Children_0_Children_1_Children_1_Children[0];
      int Children_0_Children_1_Children_1_Children_0_id = Children_0_Children_1_Children_1_Children_0["id"]; // 44
      const char* Children_0_Children_1_Children_1_Children_0_Text = Children_0_Children_1_Children_1_Children_0["Text"]; // "CPU Core #1"
      
      const char* Children_0_Children_1_Children_1_Children_0_Min = Children_0_Children_1_Children_1_Children_0["Min"]; // "41,0 °C"
      const char* Children_0_Children_1_Children_1_Children_0_Value = Children_0_Children_1_Children_1_Children_0["Value"]; // "44,0 °C"
      const char* Children_0_Children_1_Children_1_Children_0_Max = Children_0_Children_1_Children_1_Children_0["Max"]; // "67,0 °C"
      const char* Children_0_Children_1_Children_1_Children_0_ImageURL = Children_0_Children_1_Children_1_Children_0["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_1_Children_1_Children_1 = Children_0_Children_1_Children_1_Children[1];
      int Children_0_Children_1_Children_1_Children_1_id = Children_0_Children_1_Children_1_Children_1["id"]; // 45
      const char* Children_0_Children_1_Children_1_Children_1_Text = Children_0_Children_1_Children_1_Children_1["Text"]; // "CPU Core #2"
      
      const char* Children_0_Children_1_Children_1_Children_1_Min = Children_0_Children_1_Children_1_Children_1["Min"]; // "40,0 °C"
      const char* Children_0_Children_1_Children_1_Children_1_Value = Children_0_Children_1_Children_1_Children_1["Value"]; // "43,0 °C"
      const char* Children_0_Children_1_Children_1_Children_1_Max = Children_0_Children_1_Children_1_Children_1["Max"]; // "67,0 °C"
      const char* Children_0_Children_1_Children_1_Children_1_ImageURL = Children_0_Children_1_Children_1_Children_1["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_1_Children_1_Children_2 = Children_0_Children_1_Children_1_Children[2];
      int Children_0_Children_1_Children_1_Children_2_id = Children_0_Children_1_Children_1_Children_2["id"]; // 46
      const char* Children_0_Children_1_Children_1_Children_2_Text = Children_0_Children_1_Children_1_Children_2["Text"]; // "CPU Core #3"
      
      const char* Children_0_Children_1_Children_1_Children_2_Min = Children_0_Children_1_Children_1_Children_2["Min"]; // "39,0 °C"
      const char* Children_0_Children_1_Children_1_Children_2_Value = Children_0_Children_1_Children_1_Children_2["Value"]; // "41,0 °C"
      const char* Children_0_Children_1_Children_1_Children_2_Max = Children_0_Children_1_Children_1_Children_2["Max"]; // "65,0 °C"
      const char* Children_0_Children_1_Children_1_Children_2_ImageURL = Children_0_Children_1_Children_1_Children_2["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_1_Children_1_Children_3 = Children_0_Children_1_Children_1_Children[3];
      int Children_0_Children_1_Children_1_Children_3_id = Children_0_Children_1_Children_1_Children_3["id"]; // 47
      const char* Children_0_Children_1_Children_1_Children_3_Text = Children_0_Children_1_Children_1_Children_3["Text"]; // "CPU Core #4"
      
      const char* Children_0_Children_1_Children_1_Children_3_Min = Children_0_Children_1_Children_1_Children_3["Min"]; // "39,0 °C"
      const char* Children_0_Children_1_Children_1_Children_3_Value = Children_0_Children_1_Children_1_Children_3["Value"]; // "42,0 °C"
      const char* Children_0_Children_1_Children_1_Children_3_Max = Children_0_Children_1_Children_1_Children_3["Max"]; // "68,0 °C"
      const char* Children_0_Children_1_Children_1_Children_3_ImageURL = Children_0_Children_1_Children_1_Children_3["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_1_Children_1_Children_4 = Children_0_Children_1_Children_1_Children[4];
      int Children_0_Children_1_Children_1_Children_4_id = Children_0_Children_1_Children_1_Children_4["id"]; // 48
      const char* Children_0_Children_1_Children_1_Children_4_Text = Children_0_Children_1_Children_1_Children_4["Text"]; // "CPU Package"
      
      const char* Children_0_Children_1_Children_1_Children_4_Min = Children_0_Children_1_Children_1_Children_4["Min"]; // "41,0 °C"
      const char* Children_0_Children_1_Children_1_Children_4_Value = Children_0_Children_1_Children_1_Children_4["Value"]; // "44,0 °C"
      const char* Children_0_Children_1_Children_1_Children_4_Max = Children_0_Children_1_Children_1_Children_4["Max"]; // "68,0 °C"
      const char* Children_0_Children_1_Children_1_Children_4_ImageURL = Children_0_Children_1_Children_1_Children_4["ImageURL"]; // "images/transparent.png"
      
      const char* Children_0_Children_1_Children_1_Min = Children_0_Children_1_Children_1["Min"]; // ""
      const char* Children_0_Children_1_Children_1_Value = Children_0_Children_1_Children_1["Value"]; // ""
      const char* Children_0_Children_1_Children_1_Max = Children_0_Children_1_Children_1["Max"]; // ""
      const char* Children_0_Children_1_Children_1_ImageURL = Children_0_Children_1_Children_1["ImageURL"]; // "images_icon/temperature.png"
      
      JsonObject Children_0_Children_1_Children_2 = Children_0_Children_1_Children[2];
      int Children_0_Children_1_Children_2_id = Children_0_Children_1_Children_2["id"]; // 49
      const char* Children_0_Children_1_Children_2_Text = Children_0_Children_1_Children_2["Text"]; // "Load"
      
      JsonArray Children_0_Children_1_Children_2_Children = Children_0_Children_1_Children_2["Children"];
      
      JsonObject Children_0_Children_1_Children_2_Children_0 = Children_0_Children_1_Children_2_Children[0];
      int Children_0_Children_1_Children_2_Children_0_id = Children_0_Children_1_Children_2_Children_0["id"]; // 50
      const char* Children_0_Children_1_Children_2_Children_0_Text = Children_0_Children_1_Children_2_Children_0["Text"]; // "CPU Total"
      
      const char* Children_0_Children_1_Children_2_Children_0_Min = Children_0_Children_1_Children_2_Children_0["Min"]; // "5,5 %"
      const char* Children_0_Children_1_Children_2_Children_0_Value = Children_0_Children_1_Children_2_Children_0["Value"]; // "44,1 %"
      const char* Children_0_Children_1_Children_2_Children_0_Max = Children_0_Children_1_Children_2_Children_0["Max"]; // "100,0 %"
      const char* Children_0_Children_1_Children_2_Children_0_ImageURL = Children_0_Children_1_Children_2_Children_0["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_1_Children_2_Children_1 = Children_0_Children_1_Children_2_Children[1];
      int Children_0_Children_1_Children_2_Children_1_id = Children_0_Children_1_Children_2_Children_1["id"]; // 51
      const char* Children_0_Children_1_Children_2_Children_1_Text = Children_0_Children_1_Children_2_Children_1["Text"]; // "CPU Core #1"
      
      const char* Children_0_Children_1_Children_2_Children_1_Min = Children_0_Children_1_Children_2_Children_1["Min"]; // "6,3 %"
      const char* Children_0_Children_1_Children_2_Children_1_Value = Children_0_Children_1_Children_2_Children_1["Value"]; // "48,4 %"
      const char* Children_0_Children_1_Children_2_Children_1_Max = Children_0_Children_1_Children_2_Children_1["Max"]; // "100,0 %"
      const char* Children_0_Children_1_Children_2_Children_1_ImageURL = Children_0_Children_1_Children_2_Children_1["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_1_Children_2_Children_2 = Children_0_Children_1_Children_2_Children[2];
      int Children_0_Children_1_Children_2_Children_2_id = Children_0_Children_1_Children_2_Children_2["id"]; // 52
      const char* Children_0_Children_1_Children_2_Children_2_Text = Children_0_Children_1_Children_2_Children_2["Text"]; // "CPU Core #2"
      
      const char* Children_0_Children_1_Children_2_Children_2_Min = Children_0_Children_1_Children_2_Children_2["Min"]; // "3,1 %"
      const char* Children_0_Children_1_Children_2_Children_2_Value = Children_0_Children_1_Children_2_Children_2["Value"]; // "42,2 %"
      const char* Children_0_Children_1_Children_2_Children_2_Max = Children_0_Children_1_Children_2_Children_2["Max"]; // "100,0 %"
      const char* Children_0_Children_1_Children_2_Children_2_ImageURL = Children_0_Children_1_Children_2_Children_2["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_1_Children_2_Children_3 = Children_0_Children_1_Children_2_Children[3];
      int Children_0_Children_1_Children_2_Children_3_id = Children_0_Children_1_Children_2_Children_3["id"]; // 53
      const char* Children_0_Children_1_Children_2_Children_3_Text = Children_0_Children_1_Children_2_Children_3["Text"]; // "CPU Core #3"
      
      const char* Children_0_Children_1_Children_2_Children_3_Min = Children_0_Children_1_Children_2_Children_3["Min"]; // "1,6 %"
      const char* Children_0_Children_1_Children_2_Children_3_Value = Children_0_Children_1_Children_2_Children_3["Value"]; // "42,2 %"
      const char* Children_0_Children_1_Children_2_Children_3_Max = Children_0_Children_1_Children_2_Children_3["Max"]; // "100,0 %"
      const char* Children_0_Children_1_Children_2_Children_3_ImageURL = Children_0_Children_1_Children_2_Children_3["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_1_Children_2_Children_4 = Children_0_Children_1_Children_2_Children[4];
      int Children_0_Children_1_Children_2_Children_4_id = Children_0_Children_1_Children_2_Children_4["id"]; // 54
      const char* Children_0_Children_1_Children_2_Children_4_Text = Children_0_Children_1_Children_2_Children_4["Text"]; // "CPU Core #4"
      
      const char* Children_0_Children_1_Children_2_Children_4_Min = Children_0_Children_1_Children_2_Children_4["Min"]; // "0,0 %"
      const char* Children_0_Children_1_Children_2_Children_4_Value = Children_0_Children_1_Children_2_Children_4["Value"]; // "43,8 %"
      const char* Children_0_Children_1_Children_2_Children_4_Max = Children_0_Children_1_Children_2_Children_4["Max"]; // "100,0 %"
      const char* Children_0_Children_1_Children_2_Children_4_ImageURL = Children_0_Children_1_Children_2_Children_4["ImageURL"]; // "images/transparent.png"
      
      const char* Children_0_Children_1_Children_2_Min = Children_0_Children_1_Children_2["Min"]; // ""
      const char* Children_0_Children_1_Children_2_Value = Children_0_Children_1_Children_2["Value"]; // ""
      const char* Children_0_Children_1_Children_2_Max = Children_0_Children_1_Children_2["Max"]; // ""
      const char* Children_0_Children_1_Children_2_ImageURL = Children_0_Children_1_Children_2["ImageURL"]; // "images_icon/load.png"
      
      JsonObject Children_0_Children_1_Children_3 = Children_0_Children_1_Children[3];
      int Children_0_Children_1_Children_3_id = Children_0_Children_1_Children_3["id"]; // 55
      const char* Children_0_Children_1_Children_3_Text = Children_0_Children_1_Children_3["Text"]; // "Powers"
      
      JsonArray Children_0_Children_1_Children_3_Children = Children_0_Children_1_Children_3["Children"];
      
      JsonObject Children_0_Children_1_Children_3_Children_0 = Children_0_Children_1_Children_3_Children[0];
      int Children_0_Children_1_Children_3_Children_0_id = Children_0_Children_1_Children_3_Children_0["id"]; // 56
      const char* Children_0_Children_1_Children_3_Children_0_Text = Children_0_Children_1_Children_3_Children_0["Text"]; // "CPU Package"
      
      const char* Children_0_Children_1_Children_3_Children_0_Min = Children_0_Children_1_Children_3_Children_0["Min"]; // "5,7 W"
      const char* Children_0_Children_1_Children_3_Children_0_Value = Children_0_Children_1_Children_3_Children_0["Value"]; // "12,1 W"
      const char* Children_0_Children_1_Children_3_Children_0_Max = Children_0_Children_1_Children_3_Children_0["Max"]; // "49,8 W"
      const char* Children_0_Children_1_Children_3_Children_0_ImageURL = Children_0_Children_1_Children_3_Children_0["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_1_Children_3_Children_1 = Children_0_Children_1_Children_3_Children[1];
      int Children_0_Children_1_Children_3_Children_1_id = Children_0_Children_1_Children_3_Children_1["id"]; // 57
      const char* Children_0_Children_1_Children_3_Children_1_Text = Children_0_Children_1_Children_3_Children_1["Text"]; // "CPU Cores"
      
      const char* Children_0_Children_1_Children_3_Children_1_Min = Children_0_Children_1_Children_3_Children_1["Min"]; // "1,7 W"
      const char* Children_0_Children_1_Children_3_Children_1_Value = Children_0_Children_1_Children_3_Children_1["Value"]; // "8,0 W"
      const char* Children_0_Children_1_Children_3_Children_1_Max = Children_0_Children_1_Children_3_Children_1["Max"]; // "45,5 W"
      const char* Children_0_Children_1_Children_3_Children_1_ImageURL = Children_0_Children_1_Children_3_Children_1["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_1_Children_3_Children_2 = Children_0_Children_1_Children_3_Children[2];
      int Children_0_Children_1_Children_3_Children_2_id = Children_0_Children_1_Children_3_Children_2["id"]; // 58
      const char* Children_0_Children_1_Children_3_Children_2_Text = Children_0_Children_1_Children_3_Children_2["Text"]; // "CPU Graphics"
      
      const char* Children_0_Children_1_Children_3_Children_2_Min = Children_0_Children_1_Children_3_Children_2["Min"]; // "0,0 W"
      const char* Children_0_Children_1_Children_3_Children_2_Value = Children_0_Children_1_Children_3_Children_2["Value"]; // "0,0 W"
      const char* Children_0_Children_1_Children_3_Children_2_Max = Children_0_Children_1_Children_3_Children_2["Max"]; // "0,0 W"
      const char* Children_0_Children_1_Children_3_Children_2_ImageURL = Children_0_Children_1_Children_3_Children_2["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_1_Children_3_Children_3 = Children_0_Children_1_Children_3_Children[3];
      int Children_0_Children_1_Children_3_Children_3_id = Children_0_Children_1_Children_3_Children_3["id"]; // 59
      const char* Children_0_Children_1_Children_3_Children_3_Text = Children_0_Children_1_Children_3_Children_3["Text"]; // "CPU DRAM"
      
      const char* Children_0_Children_1_Children_3_Children_3_Min = Children_0_Children_1_Children_3_Children_3["Min"]; // "0,7 W"
      const char* Children_0_Children_1_Children_3_Children_3_Value = Children_0_Children_1_Children_3_Children_3["Value"]; // "1,1 W"
      const char* Children_0_Children_1_Children_3_Children_3_Max = Children_0_Children_1_Children_3_Children_3["Max"]; // "1,7 W"
      const char* Children_0_Children_1_Children_3_Children_3_ImageURL = Children_0_Children_1_Children_3_Children_3["ImageURL"]; // "images/transparent.png"
      
      const char* Children_0_Children_1_Children_3_Min = Children_0_Children_1_Children_3["Min"]; // ""
      const char* Children_0_Children_1_Children_3_Value = Children_0_Children_1_Children_3["Value"]; // ""
      const char* Children_0_Children_1_Children_3_Max = Children_0_Children_1_Children_3["Max"]; // ""
      const char* Children_0_Children_1_Children_3_ImageURL = Children_0_Children_1_Children_3["ImageURL"]; // "images_icon/power.png"
      
      const char* Children_0_Children_1_Min = Children_0_Children_1["Min"]; // ""
      const char* Children_0_Children_1_Value = Children_0_Children_1["Value"]; // ""
      const char* Children_0_Children_1_Max = Children_0_Children_1["Max"]; // ""
      const char* Children_0_Children_1_ImageURL = Children_0_Children_1["ImageURL"]; // "images_icon/cpu.png"
      
      JsonObject Children_0_Children_2 = Children_0_Children[2];
      int Children_0_Children_2_id = Children_0_Children_2["id"]; // 60
      const char* Children_0_Children_2_Text = Children_0_Children_2["Text"]; // "Generic Memory"
      
      JsonObject Children_0_Children_2_Children_0 = Children_0_Children_2["Children"][0];
      int Children_0_Children_2_Children_0_id = Children_0_Children_2_Children_0["id"]; // 61
      const char* Children_0_Children_2_Children_0_Text = Children_0_Children_2_Children_0["Text"]; // "Load"
      
      JsonObject Children_0_Children_2_Children_0_Children_0 = Children_0_Children_2_Children_0["Children"][0];
      int Children_0_Children_2_Children_0_Children_0_id = Children_0_Children_2_Children_0_Children_0["id"]; // 62
      const char* Children_0_Children_2_Children_0_Children_0_Text = Children_0_Children_2_Children_0_Children_0["Text"]; // "Memory"
      
      const char* Children_0_Children_2_Children_0_Children_0_Min = Children_0_Children_2_Children_0_Children_0["Min"]; // "40,1 %"
      const char* Children_0_Children_2_Children_0_Children_0_Value = Children_0_Children_2_Children_0_Children_0["Value"]; // "42,9 %"
      const char* Children_0_Children_2_Children_0_Children_0_Max = Children_0_Children_2_Children_0_Children_0["Max"]; // "46,7 %"
      const char* Children_0_Children_2_Children_0_Children_0_ImageURL = Children_0_Children_2_Children_0_Children_0["ImageURL"]; // "images/transparent.png"
      
      const char* Children_0_Children_2_Children_0_Min = Children_0_Children_2_Children_0["Min"]; // ""
      const char* Children_0_Children_2_Children_0_Value = Children_0_Children_2_Children_0["Value"]; // ""
      const char* Children_0_Children_2_Children_0_Max = Children_0_Children_2_Children_0["Max"]; // ""
      const char* Children_0_Children_2_Children_0_ImageURL = Children_0_Children_2_Children_0["ImageURL"]; // "images_icon/load.png"
      
      JsonObject Children_0_Children_2_Children_1 = Children_0_Children_2["Children"][1];
      int Children_0_Children_2_Children_1_id = Children_0_Children_2_Children_1["id"]; // 63
      const char* Children_0_Children_2_Children_1_Text = Children_0_Children_2_Children_1["Text"]; // "Data"

      const char* Children_0_Children_2_Children_1_Min = Children_0_Children_2_Children_1["Min"]; // ""
      const char* Children_0_Children_2_Children_1_Value = Children_0_Children_2_Children_1["Value"]; // ""
      const char* Children_0_Children_2_Children_1_Max = Children_0_Children_2_Children_1["Max"]; // ""
      const char* Children_0_Children_2_Children_1_ImageURL = Children_0_Children_2_Children_1["ImageURL"]; // "images_icon/power.png"
      
      const char* Children_0_Children_2_Min = Children_0_Children_2["Min"]; // ""
      const char* Children_0_Children_2_Value = Children_0_Children_2["Value"]; // ""
      const char* Children_0_Children_2_Max = Children_0_Children_2["Max"]; // ""
      const char* Children_0_Children_2_ImageURL = Children_0_Children_2["ImageURL"]; // "images_icon/ram.png"
      
      JsonObject Children_0_Children_3 = Children_0_Children[3];
      int Children_0_Children_3_id = Children_0_Children_3["id"]; // 66
      const char* Children_0_Children_3_Text = Children_0_Children_3["Text"]; // "NVIDIA GeForce GTX 1650 SUPER"
      
      JsonArray Children_0_Children_3_Children = Children_0_Children_3["Children"];
      
      JsonObject Children_0_Children_3_Children_0 = Children_0_Children_3_Children[0];
      int Children_0_Children_3_Children_0_id = Children_0_Children_3_Children_0["id"]; // 67
      const char* Children_0_Children_3_Children_0_Text = Children_0_Children_3_Children_0["Text"]; // "Clocks"
      
      JsonArray Children_0_Children_3_Children_0_Children = Children_0_Children_3_Children_0["Children"];
      
      JsonObject Children_0_Children_3_Children_0_Children_0 = Children_0_Children_3_Children_0_Children[0];
      int Children_0_Children_3_Children_0_Children_0_id = Children_0_Children_3_Children_0_Children_0["id"]; // 68
      const char* Children_0_Children_3_Children_0_Children_0_Text = Children_0_Children_3_Children_0_Children_0["Text"]; // "GPU Core"
      
      const char* Children_0_Children_3_Children_0_Children_0_Min = Children_0_Children_3_Children_0_Children_0["Min"]; // "1530,0 MHz"
      const char* Children_0_Children_3_Children_0_Children_0_Value = Children_0_Children_3_Children_0_Children_0["Value"]; // "1530,0 MHz"
      const char* Children_0_Children_3_Children_0_Children_0_Max = Children_0_Children_3_Children_0_Children_0["Max"]; // "1530,0 MHz"
      const char* Children_0_Children_3_Children_0_Children_0_ImageURL = Children_0_Children_3_Children_0_Children_0["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_3_Children_0_Children_1 = Children_0_Children_3_Children_0_Children[1];
      int Children_0_Children_3_Children_0_Children_1_id = Children_0_Children_3_Children_0_Children_1["id"]; // 69
      const char* Children_0_Children_3_Children_0_Children_1_Text = Children_0_Children_3_Children_0_Children_1["Text"]; // "GPU Memory"
      
      const char* Children_0_Children_3_Children_0_Children_1_Min = Children_0_Children_3_Children_0_Children_1["Min"]; // "6001,0 MHz"
      const char* Children_0_Children_3_Children_0_Children_1_Value = Children_0_Children_3_Children_0_Children_1["Value"]; // "6001,0 MHz"
      const char* Children_0_Children_3_Children_0_Children_1_Max = Children_0_Children_3_Children_0_Children_1["Max"]; // "6001,0 MHz"
      const char* Children_0_Children_3_Children_0_Children_1_ImageURL = Children_0_Children_3_Children_0_Children_1["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_3_Children_0_Children_2 = Children_0_Children_3_Children_0_Children[2];
      int Children_0_Children_3_Children_0_Children_2_id = Children_0_Children_3_Children_0_Children_2["id"]; // 70
      const char* Children_0_Children_3_Children_0_Children_2_Text = Children_0_Children_3_Children_0_Children_2["Text"]; // "GPU Shader"
      
      const char* Children_0_Children_3_Children_0_Children_2_Min = Children_0_Children_3_Children_0_Children_2["Min"]; // "0,0 MHz"
      const char* Children_0_Children_3_Children_0_Children_2_Value = Children_0_Children_3_Children_0_Children_2["Value"]; // "0,0 MHz"
      const char* Children_0_Children_3_Children_0_Children_2_Max = Children_0_Children_3_Children_0_Children_2["Max"]; // "0,0 MHz"
      const char* Children_0_Children_3_Children_0_Children_2_ImageURL = Children_0_Children_3_Children_0_Children_2["ImageURL"]; // "images/transparent.png"
      
      const char* Children_0_Children_3_Children_0_Min = Children_0_Children_3_Children_0["Min"]; // ""
      const char* Children_0_Children_3_Children_0_Value = Children_0_Children_3_Children_0["Value"]; // ""
      const char* Children_0_Children_3_Children_0_Max = Children_0_Children_3_Children_0["Max"]; // ""
      const char* Children_0_Children_3_Children_0_ImageURL = Children_0_Children_3_Children_0["ImageURL"]; // "images_icon/clock.png"
      
      JsonObject Children_0_Children_3_Children_1 = Children_0_Children_3_Children[1];
      int Children_0_Children_3_Children_1_id = Children_0_Children_3_Children_1["id"]; // 71
      const char* Children_0_Children_3_Children_1_Text = Children_0_Children_3_Children_1["Text"]; // "Temperatures"
      
      JsonObject Children_0_Children_3_Children_1_Children_0 = Children_0_Children_3_Children_1["Children"][0];
      int Children_0_Children_3_Children_1_Children_0_id = Children_0_Children_3_Children_1_Children_0["id"]; // 72
      const char* Children_0_Children_3_Children_1_Children_0_Text = Children_0_Children_3_Children_1_Children_0["Text"]; // "GPU Core"
      
      const char* Children_0_Children_3_Children_1_Children_0_Min = Children_0_Children_3_Children_1_Children_0["Min"]; // "41,0 °C"
      const char* Children_0_Children_3_Children_1_Children_0_Value = Children_0_Children_3_Children_1_Children_0["Value"]; // "41,0 °C"
      const char* Children_0_Children_3_Children_1_Children_0_Max = Children_0_Children_3_Children_1_Children_0["Max"]; // "42,0 °C"
      const char* Children_0_Children_3_Children_1_Children_0_ImageURL = Children_0_Children_3_Children_1_Children_0["ImageURL"]; // "images/transparent.png"
      
      const char* Children_0_Children_3_Children_1_Min = Children_0_Children_3_Children_1["Min"]; // ""
      const char* Children_0_Children_3_Children_1_Value = Children_0_Children_3_Children_1["Value"]; // ""
      const char* Children_0_Children_3_Children_1_Max = Children_0_Children_3_Children_1["Max"]; // ""
      const char* Children_0_Children_3_Children_1_ImageURL = Children_0_Children_3_Children_1["ImageURL"]; // "images_icon/temperature.png"
      
      JsonObject Children_0_Children_3_Children_2 = Children_0_Children_3_Children[2];
      int Children_0_Children_3_Children_2_id = Children_0_Children_3_Children_2["id"]; // 73
      const char* Children_0_Children_3_Children_2_Text = Children_0_Children_3_Children_2["Text"]; // "Load"
      
      JsonArray Children_0_Children_3_Children_2_Children = Children_0_Children_3_Children_2["Children"];
      
      JsonObject Children_0_Children_3_Children_2_Children_0 = Children_0_Children_3_Children_2_Children[0];
      int Children_0_Children_3_Children_2_Children_0_id = Children_0_Children_3_Children_2_Children_0["id"]; // 74
      const char* Children_0_Children_3_Children_2_Children_0_Text = Children_0_Children_3_Children_2_Children_0["Text"]; // "GPU Core"
      
      const char* Children_0_Children_3_Children_2_Children_0_Min = Children_0_Children_3_Children_2_Children_0["Min"]; // "2,0 %"
      const char* Children_0_Children_3_Children_2_Children_0_Value = Children_0_Children_3_Children_2_Children_0["Value"]; // "5,0 %"
      const char* Children_0_Children_3_Children_2_Children_0_Max = Children_0_Children_3_Children_2_Children_0["Max"]; // "6,0 %"
      const char* Children_0_Children_3_Children_2_Children_0_ImageURL = Children_0_Children_3_Children_2_Children_0["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_3_Children_2_Children_1 = Children_0_Children_3_Children_2_Children[1];
      int Children_0_Children_3_Children_2_Children_1_id = Children_0_Children_3_Children_2_Children_1["id"]; // 75
      const char* Children_0_Children_3_Children_2_Children_1_Text = Children_0_Children_3_Children_2_Children_1["Text"]; // "GPU Frame Buffer"
      
      const char* Children_0_Children_3_Children_2_Children_1_Min = Children_0_Children_3_Children_2_Children_1["Min"]; // "1,0 %"
      const char* Children_0_Children_3_Children_2_Children_1_Value = Children_0_Children_3_Children_2_Children_1["Value"]; // "2,0 %"
      const char* Children_0_Children_3_Children_2_Children_1_Max = Children_0_Children_3_Children_2_Children_1["Max"]; // "3,0 %"
      const char* Children_0_Children_3_Children_2_Children_1_ImageURL = Children_0_Children_3_Children_2_Children_1["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_3_Children_2_Children_2 = Children_0_Children_3_Children_2_Children[2];
      int Children_0_Children_3_Children_2_Children_2_id = Children_0_Children_3_Children_2_Children_2["id"]; // 76
      const char* Children_0_Children_3_Children_2_Children_2_Text = Children_0_Children_3_Children_2_Children_2["Text"]; // "GPU Video Engine"
      
      const char* Children_0_Children_3_Children_2_Children_2_Min = Children_0_Children_3_Children_2_Children_2["Min"]; // "3,0 %"
      const char* Children_0_Children_3_Children_2_Children_2_Value = Children_0_Children_3_Children_2_Children_2["Value"]; // "4,0 %"
      const char* Children_0_Children_3_Children_2_Children_2_Max = Children_0_Children_3_Children_2_Children_2["Max"]; // "4,0 %"
      const char* Children_0_Children_3_Children_2_Children_2_ImageURL = Children_0_Children_3_Children_2_Children_2["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_3_Children_2_Children_3 = Children_0_Children_3_Children_2_Children[3];
      int Children_0_Children_3_Children_2_Children_3_id = Children_0_Children_3_Children_2_Children_3["id"]; // 77
      const char* Children_0_Children_3_Children_2_Children_3_Text = Children_0_Children_3_Children_2_Children_3["Text"]; // "GPU Bus Interface"
      
      const char* Children_0_Children_3_Children_2_Children_3_Min = Children_0_Children_3_Children_2_Children_3["Min"]; // "0,0 %"
      const char* Children_0_Children_3_Children_2_Children_3_Value = Children_0_Children_3_Children_2_Children_3["Value"]; // "1,0 %"
      const char* Children_0_Children_3_Children_2_Children_3_Max = Children_0_Children_3_Children_2_Children_3["Max"]; // "1,0 %"
      const char* Children_0_Children_3_Children_2_Children_3_ImageURL = Children_0_Children_3_Children_2_Children_3["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_3_Children_2_Children_4 = Children_0_Children_3_Children_2_Children[4];
      int Children_0_Children_3_Children_2_Children_4_id = Children_0_Children_3_Children_2_Children_4["id"]; // 78
      const char* Children_0_Children_3_Children_2_Children_4_Text = Children_0_Children_3_Children_2_Children_4["Text"]; // "GPU Memory"
      
      const char* Children_0_Children_3_Children_2_Children_4_Min = Children_0_Children_3_Children_2_Children_4["Min"]; // "30,4 %"
      const char* Children_0_Children_3_Children_2_Children_4_Value = Children_0_Children_3_Children_2_Children_4["Value"]; // "32,3 %"
      const char* Children_0_Children_3_Children_2_Children_4_Max = Children_0_Children_3_Children_2_Children_4["Max"]; // "34,2 %"
      const char* Children_0_Children_3_Children_2_Children_4_ImageURL = Children_0_Children_3_Children_2_Children_4["ImageURL"]; // "images/transparent.png"
      
      const char* Children_0_Children_3_Children_2_Min = Children_0_Children_3_Children_2["Min"]; // ""
      const char* Children_0_Children_3_Children_2_Value = Children_0_Children_3_Children_2["Value"]; // ""
      const char* Children_0_Children_3_Children_2_Max = Children_0_Children_3_Children_2["Max"]; // ""
      const char* Children_0_Children_3_Children_2_ImageURL = Children_0_Children_3_Children_2["ImageURL"]; // "images_icon/load.png"
      
      JsonObject Children_0_Children_3_Children_3 = Children_0_Children_3_Children[3];
      int Children_0_Children_3_Children_3_id = Children_0_Children_3_Children_3["id"]; // 79
      const char* Children_0_Children_3_Children_3_Text = Children_0_Children_3_Children_3["Text"]; // "Fans"
      
      JsonObject Children_0_Children_3_Children_3_Children_0 = Children_0_Children_3_Children_3["Children"][0];
      int Children_0_Children_3_Children_3_Children_0_id = Children_0_Children_3_Children_3_Children_0["id"]; // 80
      const char* Children_0_Children_3_Children_3_Children_0_Text = Children_0_Children_3_Children_3_Children_0["Text"]; // "GPU"
      
      const char* Children_0_Children_3_Children_3_Children_0_Min = Children_0_Children_3_Children_3_Children_0["Min"]; // "0 RPM"
      const char* Children_0_Children_3_Children_3_Children_0_Value = Children_0_Children_3_Children_3_Children_0["Value"]; // "0 RPM"
      const char* Children_0_Children_3_Children_3_Children_0_Max = Children_0_Children_3_Children_3_Children_0["Max"]; // "0 RPM"
      const char* Children_0_Children_3_Children_3_Children_0_ImageURL = Children_0_Children_3_Children_3_Children_0["ImageURL"]; // "images/transparent.png"
      
      const char* Children_0_Children_3_Children_3_Min = Children_0_Children_3_Children_3["Min"]; // ""
      const char* Children_0_Children_3_Children_3_Value = Children_0_Children_3_Children_3["Value"]; // ""
      const char* Children_0_Children_3_Children_3_Max = Children_0_Children_3_Children_3["Max"]; // ""
      const char* Children_0_Children_3_Children_3_ImageURL = Children_0_Children_3_Children_3["ImageURL"]; // "images_icon/fan.png"
      
      JsonObject Children_0_Children_3_Children_4 = Children_0_Children_3_Children[4];
      int Children_0_Children_3_Children_4_id = Children_0_Children_3_Children_4["id"]; // 81
      const char* Children_0_Children_3_Children_4_Text = Children_0_Children_3_Children_4["Text"]; // "Controls"
      
      JsonObject Children_0_Children_3_Children_4_Children_0 = Children_0_Children_3_Children_4["Children"][0];
      int Children_0_Children_3_Children_4_Children_0_id = Children_0_Children_3_Children_4_Children_0["id"]; // 82
      const char* Children_0_Children_3_Children_4_Children_0_Text = Children_0_Children_3_Children_4_Children_0["Text"]; // "GPU Fan"
      
      const char* Children_0_Children_3_Children_4_Children_0_Min = Children_0_Children_3_Children_4_Children_0["Min"]; // "42,0 %"
      const char* Children_0_Children_3_Children_4_Children_0_Value = Children_0_Children_3_Children_4_Children_0["Value"]; // "42,0 %"
      const char* Children_0_Children_3_Children_4_Children_0_Max = Children_0_Children_3_Children_4_Children_0["Max"]; // "42,0 %"
      const char* Children_0_Children_3_Children_4_Children_0_ImageURL = Children_0_Children_3_Children_4_Children_0["ImageURL"]; // "images/transparent.png"
      
      const char* Children_0_Children_3_Children_4_Min = Children_0_Children_3_Children_4["Min"]; // ""
      const char* Children_0_Children_3_Children_4_Value = Children_0_Children_3_Children_4["Value"]; // ""
      const char* Children_0_Children_3_Children_4_Max = Children_0_Children_3_Children_4["Max"]; // ""
      const char* Children_0_Children_3_Children_4_ImageURL = Children_0_Children_3_Children_4["ImageURL"]; // "images_icon/control.png"
      
      JsonObject Children_0_Children_3_Children_5 = Children_0_Children_3_Children[5];
      int Children_0_Children_3_Children_5_id = Children_0_Children_3_Children_5["id"]; // 83
      const char* Children_0_Children_3_Children_5_Text = Children_0_Children_3_Children_5["Text"]; // "Data"
      
      JsonArray Children_0_Children_3_Children_5_Children = Children_0_Children_3_Children_5["Children"];
      
      JsonObject Children_0_Children_3_Children_5_Children_0 = Children_0_Children_3_Children_5_Children[0];
      int Children_0_Children_3_Children_5_Children_0_id = Children_0_Children_3_Children_5_Children_0["id"]; // 84
      const char* Children_0_Children_3_Children_5_Children_0_Text = Children_0_Children_3_Children_5_Children_0["Text"]; // "GPU Memory Free"
      
      const char* Children_0_Children_3_Children_5_Children_0_Min = Children_0_Children_3_Children_5_Children_0["Min"]; // "2697,0 MB"
      const char* Children_0_Children_3_Children_5_Children_0_Value = Children_0_Children_3_Children_5_Children_0["Value"]; // "2771,3 MB"
      const char* Children_0_Children_3_Children_5_Children_0_Max = Children_0_Children_3_Children_5_Children_0["Max"]; // "2850,0 MB"
      const char* Children_0_Children_3_Children_5_Children_0_ImageURL = Children_0_Children_3_Children_5_Children_0["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_3_Children_5_Children_1 = Children_0_Children_3_Children_5_Children[1];
      int Children_0_Children_3_Children_5_Children_1_id = Children_0_Children_3_Children_5_Children_1["id"]; // 85
      const char* Children_0_Children_3_Children_5_Children_1_Text = Children_0_Children_3_Children_5_Children_1["Text"]; // "GPU Memory Used"
      
      const char* Children_0_Children_3_Children_5_Children_1_Min = Children_0_Children_3_Children_5_Children_1["Min"]; // "1246,0 MB"
      const char* Children_0_Children_3_Children_5_Children_1_Value = Children_0_Children_3_Children_5_Children_1["Value"]; // "1324,7 MB"
      const char* Children_0_Children_3_Children_5_Children_1_Max = Children_0_Children_3_Children_5_Children_1["Max"]; // "1399,0 MB"
      const char* Children_0_Children_3_Children_5_Children_1_ImageURL = Children_0_Children_3_Children_5_Children_1["ImageURL"]; // "images/transparent.png"
      
      JsonObject Children_0_Children_3_Children_5_Children_2 = Children_0_Children_3_Children_5_Children[2];
      int Children_0_Children_3_Children_5_Children_2_id = Children_0_Children_3_Children_5_Children_2["id"]; // 86
      const char* Children_0_Children_3_Children_5_Children_2_Text = Children_0_Children_3_Children_5_Children_2["Text"]; // "GPU Memory Total"
      
      const char* Children_0_Children_3_Children_5_Children_2_Min = Children_0_Children_3_Children_5_Children_2["Min"]; // "4096,0 MB"
      const char* Children_0_Children_3_Children_5_Children_2_Value = Children_0_Children_3_Children_5_Children_2["Value"]; // "4096,0 MB"
      const char* Children_0_Children_3_Children_5_Children_2_Max = Children_0_Children_3_Children_5_Children_2["Max"]; // "4096,0 MB"
      const char* Children_0_Children_3_Children_5_Children_2_ImageURL = Children_0_Children_3_Children_5_Children_2["ImageURL"]; // "images/transparent.png"
      
      const char* Children_0_Children_3_Children_5_Min = Children_0_Children_3_Children_5["Min"]; // ""
      const char* Children_0_Children_3_Children_5_Value = Children_0_Children_3_Children_5["Value"]; // ""
      const char* Children_0_Children_3_Children_5_Max = Children_0_Children_3_Children_5["Max"]; // ""
      const char* Children_0_Children_3_Children_5_ImageURL = Children_0_Children_3_Children_5["ImageURL"]; // "images_icon/power.png"
      
      const char* Children_0_Children_3_Min = Children_0_Children_3["Min"]; // ""
      const char* Children_0_Children_3_Value = Children_0_Children_3["Value"]; // ""
      const char* Children_0_Children_3_Max = Children_0_Children_3["Max"]; // ""
      const char* Children_0_Children_3_ImageURL = Children_0_Children_3["ImageURL"]; // "images_icon/nvidia.png"
      
      JsonObject Children_0_Children_4 = Children_0_Children[4];
      int Children_0_Children_4_id = Children_0_Children_4["id"]; // 87
      const char* Children_0_Children_4_Text = Children_0_Children_4["Text"]; // "KINGSTON SA400S37480G"
      
      
      //std::string cputemp = Children_0_Children_0_Children_0_Children_1_Children_0_Text + ": " + Children_0_Children_1_Children_1_Children_0_Value;
      //std::string cpufan = "CPU Fan: " + Children_0_Children_0_Children_0_Children_3_Children_1_Value;
      //std::string gputemp = Children_0_Children_3_Children_1_Children_0_Text + ": " + Children_0_Children_3_Children_1_Children_0_Value;
      //std::string gpufan = "GPU Fan: " + Children_0_Children_3_Children_4_Children_0_Value;
      
      //Serial.println("DonKikon-PC");
      //Serial.println(Children_0_Children_1_Text);
      //Serial.print(Children_0_Children_0_Children_0_Children_1_Children_0_Text);
      //Serial.print(": ");
      //Serial.println(Children_0_Children_1_Children_1_Children_4_Value);
      //Serial.print("CPU Fan: ");
      //Serial.println(Children_0_Children_0_Children_0_Children_3_Children_1_Value);
      //Serial.println(Children_0_Children_3_Text);
      //Serial.print(Children_0_Children_3_Children_1_Children_0_Text);
      //Serial.print(": ");
      //Serial.println(Children_0_Children_3_Children_1_Children_0_Value);
      //Serial.print("GPU Fan: ");
      //Serial.println(Children_0_Children_3_Children_4_Children_0_Value);

      display.clearDisplay();   // clear the screen and buffer
      //display.println(Children_0_Children_1_Text);
      //display.print(Children_0_Children_0_Children_0_Children_1_Children_0_Text);
      display.print("CPU L: ");
      display.println(Children_0_Children_1_Children_2_Children_0_Value);
      display.print("Temp");
      display.print(": ");
      display.println(Children_0_Children_1_Children_1_Children_4_Value);
      display.print("Fan: ");
      display.println(Children_0_Children_0_Children_0_Children_3_Children_1_Value);
      //display.println(Children_0_Children_3_Text);
      
      //display.print(Children_0_Children_3_Children_1_Children_0_Text);
      display.print("GPU L: ");
      display.println(Children_0_Children_3_Children_2_Children_0_Value);
      display.print("Temp");
      display.print(": ");
      display.println(Children_0_Children_3_Children_1_Children_0_Value);
      display.print("Fan: ");
      display.println(Children_0_Children_3_Children_4_Children_0_Value);
      display.display();
      delay(500);  // wait a second
      display.clearDisplay();
    }
 
    http.end();   //Close connection
  }
  
  delay(500);    //Send a request every 2 seconds
}

void volumenPC(long int decCode) {
  if(decCode==33464415){
      if(sensorValue<10){
          sensorValue++;
      }
  }
  else if(decCode==33478695){
      if(sensorValue>0){
          sensorValue--;
      }
  }
  
  int percent = (sensorValue*100)/10;
  //display.print(percent);
  //display.println("%");
  //display.print(sensorValue);
  //display.display();
  //delay(500);
  display.clearDisplay();

  //second row
  //lcd.setCursor(0, 1);
  char volbar[16] = {};
  for (int i=0; i<percent*0.10; i++){
    volbar[i] = 'O';
  }
  display.print("Volumen: ");
  display.println("");
  display.print(percent);
  display.println("%");
  display.print("");
  display.println("");
  display.print(volbar);
  display.display();

  //send UDP packet
  if(percent != prevpercent){ //only send packet if value has updated
    Udp.beginPacket("TU_IP", serverPort);
    char perctostr[3] = {}; //Percent to string arr
    itoa(percent, perctostr, 10);
    Udp.write(perctostr);
    Udp.endPacket();
  }

  //Displays error if wifi disconnected
  while (WiFi.status() != WL_CONNECTED){
    //lcd.setCursor(6, 0);
    display.print("WiFi disconnected.");
    Serial.print("WiFi disconnected.");
  }
  
  prevpercent = percent; //update previous value
  
  delay(350); //loop delay (mainly to keep display from flickering- need to rewrite display update func to reduce latency)
  //Display init
  display.clearDisplay();
}

void volumenPCgeneric(String controlVol){
    //display.print("Test Volumen");
  if(controlVol=="sube"){
      if(sensorValue<10){
          sensorValue++;
      }
  }
  else if(controlVol=="baja"){
      if(sensorValue>0){
          sensorValue--;
      }
  }
  
  int percent = (sensorValue*100)/10;
  display.clearDisplay();

  char volbar[16] = {};
  for (int i=0; i<percent*0.10; i++){
    volbar[i] = 'O';
  }
  display.print("Volumen: ");
  display.println("");
  display.print(percent);
  display.println("%");
  display.print("");
  display.println("");
  display.print(volbar);
  display.display();

  //send UDP packet
  if(percent != prevpercent){ //only send packet if value has updated
    Udp.beginPacket("TU_IP", serverPort);
    char perctostr[3] = {}; //Percent to string arr
    itoa(percent, perctostr, 10);
    Udp.write(perctostr);
    Udp.endPacket();
  }
  prevpercent = percent; //update previous value
  
  delay(250); //loop delay (mainly to keep display from flickering- need to rewrite display update func to reduce latency)
  //Display init
  display.clearDisplay();

}

void reloj(){
  timeClient.update();
  display.clearDisplay();
  display.print("Menu Reloj");
  display.println("");
  display.print("");
  display.println("");
  display.print("   ");
  display.print(dayWeek[timeClient.getDay()]); 
  display.println(" ");
  display.print("   ");
  display.print(timeClient.getFormattedTime());
  display.display();
  delay(1000);  
}
